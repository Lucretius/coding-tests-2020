use strict;
use warnings;

use Test::More tests => 35;


ok( six_frame("") eq "", 'six frame translation of empty string' );
ok( six_frame("A") eq "?", 'six frame translation of a single nucelotide');
ok( six_frame("TG") eq "?", 'six frame translation of a di-nucelotide');
ok( six_frame("TGA") eq "*", 'translation of a stop codon');
ok( six_frame("TGT") eq "C", 'translation of a Cysteine codon');
ok( six_frame("TGG") eq "W", 'translation of a Trytophan codon');
ok( six_frame("CGT") eq "R", 'translation of an Arginine codon');
ok( six_frame("AGG") eq "R", 'alternative translation of an Arginine codon');
ok( six_frame("AGT") eq "S", 'translation of a Serine codon');
ok( six_frame("GGG") eq "G", 'translation of a Glycine codon');
ok( six_frame("TAT") eq "Y", 'translation of a Tyrosine codon');
ok( six_frame("CAC") eq "H", 'translation of a Histidine codon');
ok( six_frame("CAA") eq "Q", 'translation of a Glutamine codon');
ok( six_frame("AAT") eq "N", 'translation of a Asparagine codon');
ok( six_frame("AAA") eq "K", 'translation of a Lysine codon');
ok( six_frame("GAT") eq "D", 'translation of a Aspartate codon');
ok( six_frame("GAA") eq "E", 'translation of a Glutamate codon');

ok(six_frame("CGTTGT") eq "RC", 'translation of dipeptide RC');
ok(six_frame("AATTGA") eq "N*", 'translation of N*');

ok(six_frame("ATG") eq "M", 'translation of a Methionine');
ok(six_frame("TTC") eq "F", 'translation of a Phenylalanine');
ok(six_frame("ATT") eq "I", 'translation of an Isoleucine');
ok(six_frame("TTG") eq "L", 'translation of a Leucine');
ok(six_frame("CTG") eq "L", 'translation of a Leucine');
ok(six_frame("GTT") eq "V", 'translation of a Valine');

ok(six_frame("TCT") eq "S", 'translation of a Serine');
ok(six_frame("CCT") eq "P", 'translation of a Proline');
ok(six_frame("ACT") eq "T", 'translation of a Threonine');
ok(six_frame("ACG") eq "T", 'alternative translation of a Threonine');
ok(six_frame("GCT") eq "A", 'translation of a Alanine');

ok(six_frame("ATGCCTATTGGATCCAAAGAGAGGCCAACATTTTTTGAAATTTTTAAGACACGCTGCAACAAAGCAG") eq "MPIGSKERPTFFEIFKTRCNKA", 'translation of partial BRCA2');

ok(six_frame("TAG") eq "x", 'amber nonsense codon');
ok(six_frame("TAA") eq "x", 'ochre nonsense codon');
ok(six_frame("TGA") eq "*", 'opal nonsense codon');

ok(six_frame("ATGCCTATTGGATC", 1) eq "", 'change translation frame to frame 1');


# Production code

sub six_frame{
	my $dna = shift;
    my $frame = shift;

    if (!defined($frame)) {
        $frame = 1;
    }

	if (length ($dna) == 0) {
		return ""
	}
	else {
		return length($dna) >= 3 ? make_translation($dna) : "?";
	}
}

sub make_translation {
    my $oligo = shift;

    my $translation = '';

    my $len_oligo = length($oligo);
    if ($len_oligo >= 3){
    	# move through the oligo 3 bases at a time
    	for (my $start_oligo = 0; $start_oligo < $len_oligo; $start_oligo += 3) {
    		my $next_codon = substr($oligo, $start_oligo, 3);
    		if ($next_codon eq 'TGA'){
        		$translation .= '*';
    		}
    		else {
        		$translation .= get_translated_codon($next_codon);
    		}
    	}
    }
    else {
        $translation = get_translated_codon($oligo);
    }
    print "\n" . $translation . "\n";
    return $translation;
    
}

sub get_translated_codon{
    my $codon = shift;

    my $translation = '';
    if (substr($codon, 1, 1) eq 'G') {
        $translation = translate_2nd_base_G($codon);
    }
    elsif (substr($codon, 1, 1) eq 'A') {
        $translation = translate_2nd_base_A($codon);
    }
    elsif (substr($codon, 1, 1) eq 'T') {
        $translation = translate_2nd_base_T($codon);
    }
    elsif (substr($codon, 1, 1) eq 'C') {
        $translation = translate_2nd_base_C($codon);
    }

    return $translation;

}

sub translate_2nd_base_G{
    my $codon = shift;

    my $translation = 'x';
    if ($codon =~ /TG[TC]/) {
            $translation = 'C';
        }
        elsif ($codon eq 'TGG') {
            $translation = 'W';
        }
        elsif ($codon =~ /CG[TCAG]/) {
            $translation = 'R';
        }
        elsif ($codon =~ /AG[AG]/) {
            $translation = 'R';
        }
        elsif ($codon =~ /AG[TC]/) {
            $translation = 'S';
        }
        elsif ($codon =~ /GG[TCAG]/) {
            $translation = 'G';
        }
    return $translation;
}

sub translate_2nd_base_A{
    my $codon = shift;

    my $translation = 'x';
    if ($codon =~ /TA[TC]/) {
            $translation = 'Y';
        }
        elsif ($codon =~ /CA[TC]/) {
            $translation = 'H';
        }
        elsif ($codon =~ /CA[AG]/) {
            $translation = 'Q';
        }
        elsif ($codon =~ /AA[TC]/) {
            $translation = 'N';
        }
        elsif ($codon =~ /AA[AG]/) {
            $translation = 'K';
        }
        elsif ($codon =~ /GA[TC]/) {
            $translation = 'D';
        }
        elsif ($codon =~ /GA[AG]/) {
            $translation = 'E';
        }
    return $translation;
}

sub translate_2nd_base_T{
    my $codon = shift;

    my $translation = 'x';
    if ($codon =~ /ATG/) {
            $translation = 'M';
        }
        elsif ($codon =~ /TT[TC]/) {
            $translation = 'F';
        }
        elsif ($codon =~ /AT[TCA]/) {
            $translation = 'I';
        }
        elsif ($codon =~ /TT[AG]/) {
            $translation = 'L';
        }
        elsif ($codon =~ /CT[TCAG]/) {
            $translation = 'L';
        }
        elsif ($codon =~ /GT[TCAG]/) {
            $translation = 'V';
        }
       
    return $translation;
}

sub translate_2nd_base_C{
    my $codon = shift;

    my $translation = 'x';
    if ($codon =~ /TC[TCAG]/) {
            $translation = 'S';
        }
        elsif ($codon =~ /CC[TCAG]/) {
            $translation = 'P';
        }
        elsif ($codon =~ /AC[TCAG]/) {
            $translation = 'T';
        }
        elsif ($codon =~ /GC[TCAG]/) {
            $translation = 'A';
        }
       
    return $translation;
}
