# Coding Tests 2020

Test-driven development project. Create a basic 6-frame translation tool.

The idea here is to demonstrate the utility of the test driven developmemnt approach.

In this paradigm:

1) write a failing test (Red)

2) write just sufficient production code to make the test pass (Green)

3) refactor (Refactor

Repeat 1, 2 & 3 in a loop of development.

As the tests become more specific, the production code becomes more general.
